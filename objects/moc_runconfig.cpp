/****************************************************************************
** Meta object code from reading C++ file 'runconfig.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../runconfig/runconfig.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/qplugin.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'runconfig.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_RunConfig_t {
    QByteArrayData data[40];
    char stringdata0[468];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_RunConfig_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_RunConfig_t qt_meta_stringdata_RunConfig = {
    {
QT_MOC_LITERAL(0, 0, 9), // "RunConfig"
QT_MOC_LITERAL(1, 10, 22), // "moduleBoardTypeChanged"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 17), // "partNumberChanged"
QT_MOC_LITERAL(4, 52, 13), // "actionChanged"
QT_MOC_LITERAL(5, 66, 14), // "memSizeChanged"
QT_MOC_LITERAL(6, 81, 15), // "memWidthChanged"
QT_MOC_LITERAL(7, 97, 15), // "memClockChanged"
QT_MOC_LITERAL(8, 113, 18), // "setModuleBoardType"
QT_MOC_LITERAL(9, 132, 3), // "mbt"
QT_MOC_LITERAL(10, 136, 13), // "setPartNumber"
QT_MOC_LITERAL(11, 150, 2), // "pn"
QT_MOC_LITERAL(12, 153, 9), // "setAction"
QT_MOC_LITERAL(13, 163, 3), // "act"
QT_MOC_LITERAL(14, 167, 10), // "setMemSize"
QT_MOC_LITERAL(15, 178, 4), // "size"
QT_MOC_LITERAL(16, 183, 11), // "setMemWidth"
QT_MOC_LITERAL(17, 195, 5), // "width"
QT_MOC_LITERAL(18, 201, 11), // "setMemClock"
QT_MOC_LITERAL(19, 213, 3), // "clk"
QT_MOC_LITERAL(20, 217, 17), // "setExecutablePath"
QT_MOC_LITERAL(21, 235, 8), // "execPath"
QT_MOC_LITERAL(22, 244, 18), // "getModuleBoardType"
QT_MOC_LITERAL(23, 263, 13), // "getPartNumber"
QT_MOC_LITERAL(24, 277, 9), // "getAction"
QT_MOC_LITERAL(25, 287, 10), // "getMemSize"
QT_MOC_LITERAL(26, 298, 11), // "getMemWidth"
QT_MOC_LITERAL(27, 310, 11), // "getMemClock"
QT_MOC_LITERAL(28, 322, 14), // "printRunConfig"
QT_MOC_LITERAL(29, 337, 19), // "setProcessArguments"
QT_MOC_LITERAL(30, 357, 10), // "runProcess"
QT_MOC_LITERAL(31, 368, 19), // "parseXmlBasicConfig"
QT_MOC_LITERAL(32, 388, 10), // "fileExists"
QT_MOC_LITERAL(33, 399, 8), // "filePath"
QT_MOC_LITERAL(34, 408, 15), // "moduleBoardType"
QT_MOC_LITERAL(35, 424, 10), // "partNumber"
QT_MOC_LITERAL(36, 435, 6), // "action"
QT_MOC_LITERAL(37, 442, 7), // "memSize"
QT_MOC_LITERAL(38, 450, 8), // "memWidth"
QT_MOC_LITERAL(39, 459, 8) // "memClock"

    },
    "RunConfig\0moduleBoardTypeChanged\0\0"
    "partNumberChanged\0actionChanged\0"
    "memSizeChanged\0memWidthChanged\0"
    "memClockChanged\0setModuleBoardType\0"
    "mbt\0setPartNumber\0pn\0setAction\0act\0"
    "setMemSize\0size\0setMemWidth\0width\0"
    "setMemClock\0clk\0setExecutablePath\0"
    "execPath\0getModuleBoardType\0getPartNumber\0"
    "getAction\0getMemSize\0getMemWidth\0"
    "getMemClock\0printRunConfig\0"
    "setProcessArguments\0runProcess\0"
    "parseXmlBasicConfig\0fileExists\0filePath\0"
    "moduleBoardType\0partNumber\0action\0"
    "memSize\0memWidth\0memClock"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_RunConfig[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      24,   14, // methods
       6,  178, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  134,    2, 0x06 /* Public */,
       3,    0,  135,    2, 0x06 /* Public */,
       4,    0,  136,    2, 0x06 /* Public */,
       5,    0,  137,    2, 0x06 /* Public */,
       6,    0,  138,    2, 0x06 /* Public */,
       7,    0,  139,    2, 0x06 /* Public */,

 // methods: name, argc, parameters, tag, flags
       8,    1,  140,    2, 0x02 /* Public */,
      10,    1,  143,    2, 0x02 /* Public */,
      12,    1,  146,    2, 0x02 /* Public */,
      14,    1,  149,    2, 0x02 /* Public */,
      16,    1,  152,    2, 0x02 /* Public */,
      18,    1,  155,    2, 0x02 /* Public */,
      20,    1,  158,    2, 0x02 /* Public */,
      22,    0,  161,    2, 0x02 /* Public */,
      23,    0,  162,    2, 0x02 /* Public */,
      24,    0,  163,    2, 0x02 /* Public */,
      25,    0,  164,    2, 0x02 /* Public */,
      26,    0,  165,    2, 0x02 /* Public */,
      27,    0,  166,    2, 0x02 /* Public */,
      28,    0,  167,    2, 0x02 /* Public */,
      29,    0,  168,    2, 0x02 /* Public */,
      30,    0,  169,    2, 0x02 /* Public */,
      31,    2,  170,    2, 0x02 /* Public */,
      32,    1,  175,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,   11,
    QMetaType::Void, QMetaType::QString,   13,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void, QMetaType::QString,   17,
    QMetaType::Void, QMetaType::QString,   19,
    QMetaType::Bool, QMetaType::QString,   21,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    2,    2,
    QMetaType::Bool, QMetaType::QString,   33,

 // properties: name, type, flags
      34, QMetaType::QString, 0x00495103,
      35, QMetaType::QString, 0x00495103,
      36, QMetaType::QString, 0x00495103,
      37, QMetaType::QString, 0x00495103,
      38, QMetaType::QString, 0x00495103,
      39, QMetaType::QString, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,

       0        // eod
};

void RunConfig::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        RunConfig *_t = static_cast<RunConfig *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->moduleBoardTypeChanged(); break;
        case 1: _t->partNumberChanged(); break;
        case 2: _t->actionChanged(); break;
        case 3: _t->memSizeChanged(); break;
        case 4: _t->memWidthChanged(); break;
        case 5: _t->memClockChanged(); break;
        case 6: _t->setModuleBoardType((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 7: _t->setPartNumber((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->setAction((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->setMemSize((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 10: _t->setMemWidth((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 11: _t->setMemClock((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 12: { bool _r = _t->setExecutablePath((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 13: { QString _r = _t->getModuleBoardType();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 14: { QString _r = _t->getPartNumber();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 15: { QString _r = _t->getAction();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 16: { QString _r = _t->getMemSize();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 17: { QString _r = _t->getMemWidth();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 18: { QString _r = _t->getMemClock();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 19: _t->printRunConfig(); break;
        case 20: _t->setProcessArguments(); break;
        case 21: _t->runProcess(); break;
        case 22: _t->parseXmlBasicConfig((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 23: { bool _r = _t->fileExists((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (RunConfig::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&RunConfig::moduleBoardTypeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (RunConfig::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&RunConfig::partNumberChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (RunConfig::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&RunConfig::actionChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (RunConfig::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&RunConfig::memSizeChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (RunConfig::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&RunConfig::memWidthChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (RunConfig::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&RunConfig::memClockChanged)) {
                *result = 5;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        RunConfig *_t = static_cast<RunConfig *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->getModuleBoardType(); break;
        case 1: *reinterpret_cast< QString*>(_v) = _t->getPartNumber(); break;
        case 2: *reinterpret_cast< QString*>(_v) = _t->getAction(); break;
        case 3: *reinterpret_cast< QString*>(_v) = _t->getMemSize(); break;
        case 4: *reinterpret_cast< QString*>(_v) = _t->getMemWidth(); break;
        case 5: *reinterpret_cast< QString*>(_v) = _t->getMemClock(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        RunConfig *_t = static_cast<RunConfig *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setModuleBoardType(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->setPartNumber(*reinterpret_cast< QString*>(_v)); break;
        case 2: _t->setAction(*reinterpret_cast< QString*>(_v)); break;
        case 3: _t->setMemSize(*reinterpret_cast< QString*>(_v)); break;
        case 4: _t->setMemWidth(*reinterpret_cast< QString*>(_v)); break;
        case 5: _t->setMemClock(*reinterpret_cast< QString*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject RunConfig::staticMetaObject = {
    { &QQmlExtensionPlugin::staticMetaObject, qt_meta_stringdata_RunConfig.data,
      qt_meta_data_RunConfig,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *RunConfig::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *RunConfig::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_RunConfig.stringdata0))
        return static_cast<void*>(const_cast< RunConfig*>(this));
    return QQmlExtensionPlugin::qt_metacast(_clname);
}

int RunConfig::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQmlExtensionPlugin::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 24)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 24;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void RunConfig::moduleBoardTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void RunConfig::partNumberChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void RunConfig::actionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void RunConfig::memSizeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void RunConfig::memWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void RunConfig::memClockChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

QT_PLUGIN_METADATA_SECTION const uint qt_section_alignment_dummy = 42;

#ifdef QT_NO_DEBUG

QT_PLUGIN_METADATA_SECTION
static const unsigned char qt_pluginMetaData[] = {
    'Q', 'T', 'M', 'E', 'T', 'A', 'D', 'A', 'T', 'A', ' ', ' ',
    'q',  'b',  'j',  's',  0x01, 0x00, 0x00, 0x00,
    0xa0, 0x00, 0x00, 0x00, 0x0b, 0x00, 0x00, 0x00,
    0x8c, 0x00, 0x00, 0x00, 0x1b, 0x03, 0x00, 0x00,
    0x03, 0x00, 'I',  'I',  'D',  0x00, 0x00, 0x00,
    0x1c, 0x00, 'o',  'r',  'g',  '.',  'c',  'o', 
    'n',  'g',  'a',  't',  'e',  'c',  '.',  'q', 
    't',  '.',  'q',  'm',  'l',  '.',  'c',  'g', 
    't',  'M',  'F',  'G',  'u',  'i',  0x00, 0x00,
    0x1b, 0x09, 0x00, 0x00, 0x09, 0x00, 'c',  'l', 
    'a',  's',  's',  'N',  'a',  'm',  'e',  0x00,
    0x09, 0x00, 'R',  'u',  'n',  'C',  'o',  'n', 
    'f',  'i',  'g',  0x00, 0x1a, 0xe0, 0xa0, 0x00,
    0x07, 0x00, 'v',  'e',  'r',  's',  'i',  'o', 
    'n',  0x00, 0x00, 0x00, 0x11, 0x00, 0x00, 0x00,
    0x05, 0x00, 'd',  'e',  'b',  'u',  'g',  0x00,
    0x15, 0x10, 0x00, 0x00, 0x08, 0x00, 'M',  'e', 
    't',  'a',  'D',  'a',  't',  'a',  0x00, 0x00,
    0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x00,
    'p',  0x00, 0x00, 0x00, '8',  0x00, 0x00, 0x00,
    'd',  0x00, 0x00, 0x00, 'T',  0x00, 0x00, 0x00
};

#else // QT_NO_DEBUG

QT_PLUGIN_METADATA_SECTION
static const unsigned char qt_pluginMetaData[] = {
    'Q', 'T', 'M', 'E', 'T', 'A', 'D', 'A', 'T', 'A', ' ', ' ',
    'q',  'b',  'j',  's',  0x01, 0x00, 0x00, 0x00,
    0xa0, 0x00, 0x00, 0x00, 0x0b, 0x00, 0x00, 0x00,
    0x8c, 0x00, 0x00, 0x00, 0x1b, 0x03, 0x00, 0x00,
    0x03, 0x00, 'I',  'I',  'D',  0x00, 0x00, 0x00,
    0x1c, 0x00, 'o',  'r',  'g',  '.',  'c',  'o', 
    'n',  'g',  'a',  't',  'e',  'c',  '.',  'q', 
    't',  '.',  'q',  'm',  'l',  '.',  'c',  'g', 
    't',  'M',  'F',  'G',  'u',  'i',  0x00, 0x00,
    0x15, 0x09, 0x00, 0x00, 0x08, 0x00, 'M',  'e', 
    't',  'a',  'D',  'a',  't',  'a',  0x00, 0x00,
    0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x9b, 0x0c, 0x00, 0x00,
    0x09, 0x00, 'c',  'l',  'a',  's',  's',  'N', 
    'a',  'm',  'e',  0x00, 0x09, 0x00, 'R',  'u', 
    'n',  'C',  'o',  'n',  'f',  'i',  'g',  0x00,
    '1',  0x00, 0x00, 0x00, 0x05, 0x00, 'd',  'e', 
    'b',  'u',  'g',  0x00, 0x1a, 0xe0, 0xa0, 0x00,
    0x07, 0x00, 'v',  'e',  'r',  's',  'i',  'o', 
    'n',  0x00, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x00,
    '8',  0x00, 0x00, 0x00, 'T',  0x00, 0x00, 0x00,
    'p',  0x00, 0x00, 0x00, '|',  0x00, 0x00, 0x00
};
#endif // QT_NO_DEBUG

QT_MOC_EXPORT_PLUGIN(RunConfig, RunConfig)

QT_END_MOC_NAMESPACE
